<?php
if($page!="contact"){
  ?>
  <div class="container">
    <!-- Call to Action Section -->
    <div class="row mb-4 justify-content-center">
      <div class="col-md-3 text-center my-auto m-2 p-2">
        <a class="btn btn-lg btn-secondary btn-block" href="contact">Contactez nous</a>
      </div>
      <div class="col-md-3 text-center my-auto m-2 p-2">
        <a class="btn btn-lg btn-primary btn-block" href="http://chng.it/CHRwd4mk" target="_blank">Signer la pétition</a>
      </div>
    </div>
  </div>
  <?php
}
?>