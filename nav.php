  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary fixed-top">
    <div class="container">
      <a class="navbar-brand" href="./">L'Autre concertation</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link <?php echo $active["quisommesnous"];?>" href="quisommesnous">Qui sommes nous</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?php echo $active["leur-projet"];?>" href="leur-projet">Leur projet</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle <?php echo $active["l-autre-projet"];?>" href="l-autre-projet" id="navbarDropdownAutreProjet" aria-haspopup="true" aria-expanded="false">
            L'autre projet
            </a>
            <div class="dropdown-menu dropdown-menu-right my-0" aria-labelledby="navbarDropdownAutreProjet">
              <a class="dropdown-item" href="l-autre-projet#autre-choix">L'autre choix</a>
              <a class="dropdown-item" href="l-autre-projet#autre-proposition">L'autre proposition</a>
              <a class="dropdown-item" href="l-autre-projet#autre-place-de-l-arbre">L'autre place de l'arbre</a>
              <a class="dropdown-item" href="l-autre-projet#autre-fois">L'autre fois</a>
              <a class="dropdown-item" href="l-autre-projet#autre-mobilite">L'autre mobilité</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link <?php echo $active["contact"];?>" href="contact">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
