<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Laval coeur de ville : l'autre concertation</title>
  <link rel="icon" href="img/favicon.ico" />
  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

  <!-- Custom styles for this template -->
  <link href="css/lautreconcertation.css" rel="stylesheet">

</head>

<body>
<?php
$page=htmlspecialchars($_GET["p"]);
$active=array($page=>"active");

include "nav.php";
?>

<header class="pb-5">
    
</header>
<body>
<?php
$lien_adhesion="https://docs.google.com/forms/d/e/1FAIpQLSero4oEJLvys6nvFg3v3cysizIRjSfBRf9MNj53fDpcLOwF_Q/viewform?usp=sf_link";

switch ($page) {
  case 'quisommesnous':
    include("1-quisommesnous.php");
    break;
  case 'leur-projet':
    include("2-leur-projet.php");
    break;
  case 'l-autre-projet':
    include("3-l-autre-projet.php");
    break;
  case 'contact':
    include("4-contact.php");
    break;
  
  default:
    include("0-accueil.php");
    break;
}
?>
</body>

<?php
if($page!="contact"){
  ?>
  <hr>
  <div class="container">
    <!-- footer -->
    <div class="row mb-4 justify-content-center">
      <div class="col-md-3 text-center my-auto">
        Notre démarche vous intéresse ? <a href="https://www.facebook.com/Lautre-concertation-Laval-Place-du-11-Novembre-1390689161073612/?modal=admin_todo_tour"><img src="img/fb.png" alt="Lien facebook L'Autre Concertation" height="45px" width="45px"/></a>
          <a href="https://www.youtube.com/watch?v=pnudAhf3ON8&feature=youtu.be"><img src="img/yt.png" alt="Lien YouTube L'Autre Concertation" height="45px" width="45px"/></a>
      </div>
    </div>
  </div>
  <?php
}
include('call_to_action.php');
?>

  <!-- Footer
  <footer class="py-5 bg-primary">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
    </div>
  </footer> -->

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
