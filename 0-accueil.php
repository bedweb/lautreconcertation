  <!-- Page Content -->
  <div class="container">
    <h1 class="my-4">Laval Coeur de Ville : L’Autre Concertation</h1>


    <!-- Features Section -->
    <div class="row">
      <div class="col-lg-6 text-justify">
        <p>La municipalité de Laval entreprend un vaste programme de réaménagement de son Cœur de ville, notamment de la Place du 11 novembre. Cette page a pour but de vous éclairer sur ce projet et de discuter ensemble des alternatives possibles.</p>
        <p>Si vous pensez mériter mieux qu’un Coeur de Ville avec une zone commerciale grande comme la salle polyvalente, un parking souterrain en zone inondable et une « ambiance végétale » sur fond de béton ; suivez-nous et partagez cette page !</p>
        <p><a href="https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-un-collectif-s-oppose-aux-transformations-de-la-place-du-11-novembre-6359378" target="_blank" font-size="x-large">On parle de L'Autre Concertaiton sur <strong>Ouest-France</strong>...</a><br>
           <a href="http://leglob-journal.fr/a-laval-lautre-concertation-pour-un-coeur-de-ville-alternatif/?fbclid=IwAR0GQi1PuKNbVJM4PEzMHZnfP2wYYP_GGJMlx370yI7XXDHn2T9tMfjCuvc" target="_blank" font-size="x-large">... et aussi sur <strong>Leglob journal</strong></a></p>
      </div>
      <div class="col-lg-6">
        <img class="img-fluid rounded" src="img/logo.jpg" alt="">
      </div>
    </div>
    <!-- /.row -->


  </div>
  <!-- /.container -->
