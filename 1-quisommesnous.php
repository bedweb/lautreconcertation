
  <!-- Page Content -->
  <div class="container">
    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">&Agrave; propos de nous</h1>

    <!-- Intro Content -->
    <div class="row">
      <div class="col-lg-6 my-auto">
        <img class="img-fluid rounded mb-4" src="img/logo.jpg" alt="">
      </div>
      <div class="col-lg-6 text-justify my-auto">
        <p>Porté par un collectif d'une vintaine de citoyens, "L'autre Concertation"  propose aux lavallois de se mobiliser pour réfléchir à un autre projet de réaménagement de la Place du 11 novembre.</p>
      </div>
    </div>
    <!-- /.row -->


  </div>
  <!-- /.container -->
