  
  <!-- Page Content -->
  <div class="container">
    <div class="row mb-4">
      <div class="col-lg-12 mb-12 text-center">
        <p class="">Vous avez envie de travailler avec nous ou souhaitez nous contacter ? N'hésitez pas et <a class="btn btn-primary" href="<?php echo $lien_adhesion;?>" target="_blank">adhérez au mouvement</a> !</p>
        <p><a href="https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-un-collectif-s-oppose-aux-transformations-de-la-place-du-11-novembre-6359378" target="_blank" font-size="x-large">On parle de L'Autre Concertaiton sur <strong>Ouest-France</strong>...</a><br>
           <a href="http://leglob-journal.fr/a-laval-lautre-concertation-pour-un-coeur-de-ville-alternatif/?fbclid=IwAR0GQi1PuKNbVJM4PEzMHZnfP2wYYP_GGJMlx370yI7XXDHn2T9tMfjCuvc" target="_blank" font-size="x-large">... et aussi sur <strong>Leglob journal</strong></a></p>
      </div>
    </div>
    <div class="row mb-4">
      <div class="col-lg-12 mb-12 text-center">
          Contact email : <a class="btn btn-primary" href="mailto:lautreconcertation@gmail.com"><i class="far fa-envelope"></i> lautreconcertation@gmail.com   </a><a href="https://www.facebook.com/Lautre-concertation-Laval-Place-du-11-Novembre-1390689161073612/?modal=admin_todo_tour"><img src="img/fb.png" alt="Lien facebook L'Autre Concertation" height="45px" width="45px"/></a> <a href="https://www.youtube.com/watch?v=pnudAhf3ON8&feature=youtu.be"><img src="img/yt.png" alt="Lien YouTube L'Autre Concertation" height="45px" width="45px"/></a>
      </div>
    </div>

  </div>
  <!-- /.container -->
