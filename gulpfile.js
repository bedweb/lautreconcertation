// Load plugins
const browsersync = require("browser-sync").create();
const gulp = require("gulp");
const sass = require('gulp-sass');

// Copy third party libraries from /node_modules into /vendor
gulp.task('vendor', function(cb) {

  // Bootstrap
  gulp.src([
      './node_modules/bootstrap/dist/**/*',
      '!./node_modules/bootstrap/dist/css/bootstrap-grid*',
      '!./node_modules/bootstrap/dist/css/bootstrap-reboot*'
    ])
    .pipe(gulp.dest('./vendor/bootstrap'))

  // jQuery
  gulp.src([
      './node_modules/jquery/dist/*',
      '!./node_modules/jquery/dist/core.js'
    ])
    .pipe(gulp.dest('./vendor/jquery'))

  cb();

});

// Compile SCSS
gulp.task('css:compile', function() {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass.sync({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(gulp.dest('./css'))
});

// BrowserSync
function browserSync(done) {
  browsersync.init({
    /*server: {
      baseDir: "./"
    }*/
    proxy: '127.0.0.1:80',
    startPath:"/lautreconcertation.fr"
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// Watch files
function watchFiles() {
  gulp.watch("./css/*", browserSyncReload);
  gulp.watch("./**/*.html", browserSyncReload);
  gulp.watch("./**/*.php", browserSyncReload);
}

gulp.task("default", gulp.parallel('vendor','css:compile'));

// dev task
gulp.task("watch", gulp.parallel(watchFiles, browserSync,'css:compile'));
