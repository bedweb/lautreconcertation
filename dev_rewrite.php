<?php

// fichier utilisé pour le développement
// pour gérer la réécriture d'URL avec le serveur php
// php -S localhost:8000 dev_rewrite.php


$indexFiles = ['index.html', 'index.php'];
$routes = [
  '([^/]+)$' => '/index.php'
];

$requestedAbsoluteFile = dirname(__FILE__) . DIRECTORY_SEPARATOR . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
//$requestedRelativeFile = basename($_SERVER['REQUEST_URI'], '?'.$_SERVER['QUERY_STRING']);
//echo $requestedAbsoluteFile."<br>";

if(preg_match('/index\.php$/', $requestedAbsoluteFile)){
    header('Content-Type: '.mime_content_type($requestedAbsoluteFile));
    echo "<div></div>";//forcer le début de la sortie sans DOCTYPE html pour éviter un bug avec le css ...
    include_once $requestedAbsoluteFile;
}else if (is_file($requestedAbsoluteFile)){
    header('Content-Type: '.mime_content_type($requestedAbsoluteFile));
    $fh = fopen($requestedAbsoluteFile, 'r');
    fpassthru($fh);
    fclose($fh);
    return true;
}else{
    foreach ($routes as $regex => $fn){
        if (preg_match('%'.$regex.'%', $_SERVER['REQUEST_URI'],$matches)){
            $requestedAbsoluteFile = dirname(__FILE__) . $fn ;
            $location='index.php?p='.$matches[1];
            header('Location: '.$location); 
            return true;
            break;
        }
    }
}
?>