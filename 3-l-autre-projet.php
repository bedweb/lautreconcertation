  <?php
  include('call_to_action.php');
  ?>
  <!-- Page Content -->
  <div class="container">
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="display-4">L'autre projet</h1>
        <p class="lead">Cette page a pour vocation de rassembler des articles de toutes provenances mettant en avant des alternatives pour l'aménagement du coeur de ville de Laval.</p>
        <p>Utilisez la rubrique <a class="btn btn-secondary mx-2" href="contact">Contact</a> pour nous soumettre vos idées...</p>
      </div>
    </div>

    <!-- marge bizarre pour le lien vers l'ancre dessous -->
    <hr style="margin-bottom: -40px;">
    
    <!-- l'autre choix -->
    <div class="row mb-4">
      <div class="col-lg-12 mb-12">
        <a name="autre-choix">
        <h1 style="padding-top: 56px;">"L'autre choix" : ce qui se passe ailleurs</h1>
        </a>
        <p class="card-text text-justify">Vous êtes persuadés que la place du 11 Novembre peut être aménagée autrement ? Qu'il existe bien d'autres façons de redonner vie à un cœur de ville que de construire un parking souterrain ou un centre commercial ? Que le développement de transports doux n'est pas réservé aux "grandes" villes ? Qu'être une "petite" ville ne rend pas la voiture indispensable ? Et bien "L'autre choix" est fait pour vous !</p>
      </div>
    </div>
    <div class="row mb-4">
      <div class="col-lg-6 mb-6">
        <div class="card h-100">
          <h4 class="card-header">&Ccedil;a s'est passé à La-Roche-sur-Yon</h4>
          <img class="img-fluid mb-4" src="img/autre-projet-la-roche-sur-yon.jpg" alt="">
          <div class="card-body text-center">
            <p class="card-text text-justify">L'une des plus belles réussites de ces 10 dernières années : la place Napoléon de La-Roche-sur-Yon. Découvrez à travers les liens ci-dessous comment une ville de l'Ouest, de taille comparable à Laval, est parvenue à donner un nouvel élan à son centre-ville en étant simplement… ambitieuse ! Ici, nul parking ou bâtiment comparable à une galerie marchande de périphérie mais un véritable projet urbanistique voué à apporter quelque chose d'unique au cœur de ville, une touche culturelle tel un supplément d'âme. Vous comprendrez notamment comment la place Napoléon est devenue un espace de vie recherché, tant par les habitants que les touristes. Vous constaterez également que les opposants au projet, qui pour beaucoup se félicitent aujourd'hui de sa réalisation, étaient également persuadés que la voiture était la seule à pouvoir redynamiser le centre-ville.</p>
          </div>
          <div class="card-footer text-center">
            <a href="https://france3-regions.francetvinfo.fr/pays-de-la-loire/vendee/roche-yon-place-napoleon-attire-plus-plus-touristes-1055225.html?fbclid=IwAR36pumRxv3-nWdDgMj_VKwVxqHmPlEPUhn1rlWr5AkX5t6McBctqCIqaqY" target="_blank" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>
      </div>
    </div>
    <!-- /.l'autre choix -->

    <!-- marge bizarre pour le lien vers l'ancre dessous -->
    <hr style="margin-bottom: -40px;">

    <!-- l'autre proposition -->
    <div class="row mb-4">
      <div class="col-lg-12 mb-12">
        <a name="autre-proposition">
        <h1 style="padding-top: 56px;">"L'autre problématique des villes moyennes"</h1>
        </a>
      </div>
    </div>
    <div class="row mb-4">
      <div class="col-lg-6 mb-6">
        <div class="card h-100">
          <h4 class="card-header">« Ce n’est plus le commerce qui rend un centre-ville attractif »</h4>
          <img class="img-fluid mb-4" src="img/autre-proposition.jpg" alt="">
          <div class="card-body text-center">
            <p class="text-truncate "><a href="https://www.urbislemag.fr/-ce-n-est-plus-le-commerce-qui-rend-un-centre-ville-attractif--billet-475-urbis-le-mag.html" target="_blank" class="">https://www.urbislemag.fr/-ce-n-est-plus-le-commerce-qui-rend-un-centre-ville-attractif--billet-475-urbis-le-mag.html</a></p>
            <p class="card-text text-justify">Le programme « Action Cœur de Ville » prévoit d’investir cinq milliards d’euros pour redynamiser les villes moyennes. Afin que cet investissement puisse réellement avoir des retombées bénéfiques sur l’attractivité des villes moyennes, encore faudrait-il se poser les bonnes questions : Est-il nécessaire de créer un bâtiment commercial alors que de nombreux pas de porte sont vacants dans un centre-ville ? Les commerces n’ont-ils pas tout simplement à innover en faisant preuve d’originalité pour retrouver de l’attractivité ?</p>
          </div>
          <div class="card-footer text-center">
            <a href="https://www.urbislemag.fr/-ce-n-est-plus-le-commerce-qui-rend-un-centre-ville-attractif--billet-475-urbis-le-mag.html" target="_blank" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>
      </div>
      <div class="col-lg-6 mb-6">
        <div class="card h-100">
          <h4 class="card-header">Ces zones périphériques qui vident les centres-villes</h4>
          <img class="img-fluid mb-4" src="img/autre-proposition-2.jpg" alt="">
          <div class="card-body text-center">
            <p class="text-truncate "><a href="https://www.ieif.fr/revue_de_presse/quand-le-centre-des-villes-moyennes-se-meurt?fbclid=IwAR1Zc4-wnoFEUbReFdFal6yV5gM2Ps1ZY--kDAU7a6pM_LzKezYHT2A2ozY" target="_blank" class="">https://www.ieif.fr/revue_de_presse/quand-le-centre-des-villes-moyennes-se-meurt?fbclid=IwAR1Zc4-wnoFEUbReFdFal6yV5gM2Ps1ZY--kDAU7a6pM_LzKezYHT2A2ozY</a></p>
            <p class="card-text text-justify">
« Les surfaces commerciales connaissent une croissance plus rapide que la population. » Constat édifiant d’autant plus lorsque l’on sait que la croissances des mètres carrés est supérieure à celle de la consommation ! Et Le Monde explique cette aberration de la sorte : « Les nouvelles opérations ne répondent pas nécessairement à un besoin des clients, mais sont présentées par les élus qui les défendent comme des pourvoyeurs d’activité, de croissance et d’emploi. »… LAVAL est loin d’être épargnée : alors qu’en France, la densité commerciale moyenne d’hyper et de supermarchés est de 373m² pour 1000habitants, LAVAL fait partie des villes où la densité se situe entre 400 et 500m² pour 1000habitants. Alors posons-nous la question : Plutôt que d’ajouter 6000m² de surface commerciale à travers la construction d’un bâtiment onéreux sur notre place, ne faudrait-il pas prendre les problèmes dans l’ordre et davantage cadrer le développement de ces zones périphériques afin de lutter contre ce suréquipement ?</p>
          </div>
          <div class="card-footer text-center">
            <a href="https://www.ieif.fr/revue_de_presse/quand-le-centre-des-villes-moyennes-se-meurt?fbclid=IwAR1Zc4-wnoFEUbReFdFal6yV5gM2Ps1ZY--kDAU7a6pM_LzKezYHT2A2ozY" target="_blank" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>
      </div>
    </div>
    <!-- /.l'autre proposition -->

    <!-- marge bizarre pour le lien vers l'ancre dessous -->
    <hr style="margin-bottom: -40px;">

    <!-- l'autre place de l'arbre -->
    <div class="row mb-4">
      <div class="col-lg-12 mb-12">
        <a name="autre-place-de-l-arbre">
        <h1 style="padding-top: 56px;">"L'autre place de l'arbre" dans la ville</h1>
        </a>
      </div>
    </div>
    <div class="row mb-4">
      <div class="col-lg-6 mb-6">
        <div class="card h-100">
          <h4 class="card-header">Le rôle des arbres en ville</h4>
          <img class="img-fluid mb-4" src="img/autre-place-arbre-1.jpg" alt="">
          <div class="card-body text-center">
            <p class="text-truncate "><a href="https://www.pourlascience.fr/sd/biologie/comme-un-arbre-dans-la-ville-14847.php" target="_blank" class="">https://www.pourlascience.fr/sd/biologie/comme-un-arbre-dans-la-ville-14847.php</a></p>
            <p class="card-text text-justify">Le projet d’aménagement du « Coeur de Ville » devrait conduire à l’abattage des 120 arbres actuellement présents sur la Place du 11 novembre.</p>
            <p class="card-text text-justify">« L’ambiance végétale » promise en contrepartie semble bien peu ambitieuse tant les arbres génèrent une multitude de bienfaits. Ils participent au maintien de la biodiversité, en hébergeant une faune considérable. Ils diminuent les risques d’inondations suite aux orages (dont on connait les effets dans la rue du Général de Gaulle). Ils participent au bien-être de la population, des enfants jusqu’aux personnes âgées. Et puis bien sûr, ils apportent une protection salutaire lors des chaleurs d’été : les écarts de température peuvent varier de 2 à 5°C (et jusqu’à 10° en température ressentie) entre une rue dépourvue de couvert végétal et un espace vert.</p>
            <p class="card-text text-justify">Ils constituent ainsi des instruments efficaces contre les « îlots de chaleur urbains » : ils forment des obstacles physiques à la lumière et apportent un ombrage protecteur (jusqu’à 85% des rayons peuvent être filtrés). Ils puisent de l’eau en profondeur grâce à leurs racines et émettent dans l’air cette humidité rafraichissante.</p>
            <p class="card-text text-justify">Par ailleurs, les arbres participent aussi au maintien de la qualité de l’air : leur feuillage filtre l’air et réduit notablement le niveau de particules dans l’espace alentour. Enfin, les arbres sont des réservoirs à carbone : la photosynthèse leur permet d’absorber le carbone atmosphérique.</p>
          </div>
          <div class="card-footer text-center">
            <a href="https://www.pourlascience.fr/sd/biologie/comme-un-arbre-dans-la-ville-14847.php" target="_blank" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>
      </div>
      <div class="col-lg-6 mb-6">
        <div class="card h-100">
          <h4 class="card-header">L'arbre dans les villes du monde entier</h4>
          <img class="img-fluid mb-4" src="img/autre-place-arbre-2.jpg" alt="">
          <div class="card-body text-center">
            <p class="text-truncate "><a href="https://www.20minutes.fr/planete/2318167-20180806-canicule-comment-reinventer-ville-faire-face-chaleurs-etouffantes" target="_blank" class="">https://www.20minutes.fr/planete/2318167-20180806-canicule-comment-reinventer-ville-faire-face-chaleurs-etouffantes</a></p>
            <p class="card-text text-justify">Ailleurs dans le monde, quelle place les villes font-elles aux arbres ? De grandes villes se sont lancées dans des projets ambitieux d’introduction des arbres dans la ville. New York a lancé sont « Million tree initiative » dès 2007 et a achevé ce programme moins de 10 ans plus tard, en 2015. Les effets de cette politique new-yorkaise ont donné lieu à des travaux scientifiques confirmant l’impact important de la couverture arborée.</p>
            <p class="card-text text-justify">Toujours aux Etats-Unis, Boston, Los Angeles, Toronto, Seattle ont imité New York, ou encore Louisville dans le Kentucky. Là, la température augmente d’année en année plus vite qu’ailleurs dans le pays et la mairie a fait le choix de lancer un programme de plantation d’arbres pour lutter contre les îlots de chaleur urbains. Ailleurs dans le monde, à Montréal (Plan d’action Canopée), Shanghai ou Londres, de projets comparables sont en cours...</p>
          </div>
          <div class="card-footer text-center">
            <a href="https://www.20minutes.fr/planete/2318167-20180806-canicule-comment-reinventer-ville-faire-face-chaleurs-etouffantes" target="_blank" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>
      </div>
    </div>
    <!-- /.l'autre place de l'arbre -->

    <!-- marge bizarre pour le lien vers l'ancre dessous -->
    <hr style="margin-bottom: -40px;">
    
    <!-- l'autre fois ça n'a pas marché -->
    <div class="row mb-4">
      <div class="col-lg-12 mb-12">
        <a name="autre-fois">
        <h1 style="padding-top: 56px;">"L'autre fois" ça n'a pas marché</h1>
        </a>
      </div>
    </div>
    <div class="row mb-4">
      <div class="col-lg-6 mb-6">
        <div class="card h-100">
          <h4 class="card-header">&Ccedil;a s'est passé à St Brieuc</h4>
          <img class="img-fluid mb-4" src="img/autre-fois-1.jpg" alt="">
          <div class="card-body text-center">
            <p class="text-truncate "><a href="https://www.letelegramme.fr/bretagne/halles-detruites-saint-brieuc-respire-18-05-2016-11071450.php" target="_blank" class="">https://www.letelegramme.fr/bretagne/halles-detruites-saint-brieuc-respire-18-05-2016-11071450</a></p>
            <p class="card-text text-justify">Le projet présenté n'a rien de novateur, la preuve par neuf. D'autres villes ont expérimenté la création de grandes surfaces commerciales en coeur de ville depuis plus de 30 ans. Pour quels résultats ? Sevons nous de ces expériences pour ne pas faire les mêmes erreurs.</p>
          </div>
          <div class="card-footer text-center">
            <a href="https://www.letelegramme.fr/bretagne/halles-detruites-saint-brieuc-respire-18-05-2016-11071450.php" target="_blank" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>
      </div>
      <div class="col-lg-6 mb-6">
        <div class="card h-100">
          <h4 class="card-header">&Ccedil;a se passe à Cholet et Angoulême</h4>
          <img class="img-fluid mb-4" src="img/autre-fois-2.jpg" alt="">
          <div class="card-body text-center">
           <p class="text-truncate "><a href="https://www.ouest-france.fr/pays-de-la-loire/cholet-49300/cholet-ces-rues-du-centre-ou-le-commerce-est-en-berne-6240906" target="_blank" class="">https://www.ouest-france.fr/pays-de-la-loire/cholet-49300/cholet-ces-rues-du-centre-ou-le-commerce-est-en-berne-6240906</a></p>
            <p class="text-truncate "><a href="https://www.charentelibre.fr/2017/03/23/champ-de-mars-a-angouleme-dix-ans-a-tenter-d-epater-la-galerie,3092426.php" target="_blank" class="">https://www.charentelibre.fr/2017/03/23/champ-de-mars-a-angouleme-dix-ans-a-tenter-d-epater-la-galerie,3092426</a></p>
          </div>
          <div class="card-footer text-center">
            <a href="https://www.charentelibre.fr/2017/03/23/champ-de-mars-a-angouleme-dix-ans-a-tenter-d-epater-la-galerie,3092426.php" target="_blank" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>
      </div>
    </div>
    <!-- /.l'autre fois ca n'a pas marché -->

    <!-- l'autre mobilité -->
    <div class="row mb-4">
      <div class="col-lg-12 mb-12">
        <a name="autre-mobilite">
        <h1 style="padding-top: 56px;">"L'autre mobilité"</h1>
        </a>
      </div>
    </div>
    <div class="row mb-4">
      <div class="col-lg-6 mb-6">
        <div class="card h-100">
          <h4 class="card-header">Se déplacer autrement en ville</h4>
          <img class="img-fluid mb-4" src="img/autre-mobilite.jpg" alt="">
          <div class="card-body text-center">
            <p class="text-truncate "><a href="https://www.pourlascience.fr/sd/biologie/comme-un-arbre-dans-la-ville-14847.php" target="_blank" class="">https://www.pourlascience.fr/sd/biologie/comme-un-arbre-dans-la-ville-14847.php</a></p>
            <p class="card-text text-justify"> Des solutions alternatives existent pour se garer en périphérie du centre-ville (Ferrié, Bourny, Hilard, Gare...) tout en garantissant son accès : navettes, vélos, trottinettes, taxis-vélos... Il serait bon de proposer des pistes cyclables pour faciliter et sécuriser le passage à ces modes de déplacement très bénéfiques : pour la santé, pour l'environnement, pour le bruit... De plus, des lignes de bus réguliers couvrent la première et la deuxième couronne de la ville. De quoi alléger le trafic !</p>
            <p class="card-text text-justify">On peut aussi opter pour les navettes à la demande ou le covoiturage, à une époque où le bien-vivre se traduit aussi par le retour au lien social.
Actuellement, les projets de la ville s'axent sur l'automobile, quand une voiture peut coûter de 5000€ à 8000€ par an, que les familles en agglomération en ont souvent deux et que le carburant va de toute façon augmenter...!
Pour ceux qui n'habitent pas Laval et souhaitent profiter du centre, il y a à rappeler que le retour du rail est désormais très attendu...</p>
            <p class="card-text text-justify">D'autre part, la pollution sonore peut représenter un danger pour notre santé, et abîmer le charme tranquille d'un cœur de ville...: <a href="https://www.topsante.com/medecine/environnement-et-sante/pollution/la-pollution-sonore-a-un-impact-sur-notre-sante-61449?fbclid=IwAR1dkSLxKryj6R4p3AZ8YDYgTvchYiWDSULRjC-d66vH0rUY3QEtvtMa6ns"target="_blank" class="">voir le lien</a></p>
            <p class="card-text text-justify">Une liste des possibilités de déplacement alternatif déjà existantes (non-exhaustive) : <a href="https://www.consoglobe.com/10-modes-de-transport-propres-pour-se-deplacer-en-ville-cg?fbclid=IwAR2nByZKwgEXfngzqGkbbRU2aA0OHVIIGnJVECkKGp-lxfBaGZtP56rx7d8"target="_blank" class="">voir le lien</a></p>
            <p class="card-text text-justify">Cet article très inspirant nous présente une écolo-crèche en centre-ville, qui fait le bonheur de ses petits résidents et de leurs parents depuis 10 ans ! : <a href="https://lesprosdelapetiteenfance.fr/initiatives/developpement-durable/cafarandole-une-ecolo-creche-en-plein-centre-ville?fbclid=IwAR1c5RVE53uiGhakK7LRzc30nrE-Y8xHqPBANwlZCEGqzmxscJj7TUweE7E"target="_blank" class="">voir le lien</a></p>
            <p class="card-text text-justify">A lire absolument ! "Piétons et cyclistes, meilleurs atouts pour le commerce de proximité ?" : <a href="http://territoires.blog.lemonde.fr/2013/03/29/pietons-et-cyclistes-meilleurs-atouts-pour-les-commerces-de-proximite/?fbclid=IwAR1jtTkJ7eSNSrPSQJqIMlHxTwMXCtyAm27iDJcQz2UmFZVabSDhJJzeWsc"target="_blank" class="">voir le lien</a></p>
            <p class="card-text text-justify">L'excellent exemple Québécois (encore!) : <a href="https://www.consoglobe.com/automobile-montreal-stationnement-transition-douce-cg?fbclid=IwAR26S1fkqwP4oxPTsbRxfN3jlNfkE00gbUuckTFXGfn1dW4QHs3APhqD_4U"target="_blank" class="">voir le lien</a></p>
          </div>
          <div class="card-footer text-center">
            <a href="https://www.pourlascience.fr/sd/biologie/comme-un-arbre-dans-la-ville-14847.php" target="_blank" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>
      </div>
      <div class="col-lg-6 mb-6">
        <div class="card h-100">
          <h4 class="card-header">Quelles mobilités à l'avenir ?</h4>
          <img class="img-fluid mb-4" src="img/autre-mobilite-2.jpg" alt="">
          <div class="card-body text-center">
            <p class="text-truncate "><a href="http://www.qqf.fr/infographie/60/la-mobilite-de-demain?fbclid=IwAR2RWnyg-pYgRbUkvC3FWUeUpuPmCIMfluNgXSFEjrre8UHvDusDLM6nEc8" target="_blank" class="">http://www.qqf.fr/infographie/60/la-mobilite-de-demain?fbclid=IwAR2RWnyg-pYgRbUkvC3FWUeUpuPmCIMfluNgXSFEjrre8UHvDusDLM6nEc8</a></p>
            <p class="card-text text-justify">« Qu’est-ce qu’on fait ?! » propose une infographie ludique faisant le point sur la situation actuelle puis propose des alternatives possibles concernant la mobilité. Il est temps de se poser la question ; Plus d’1 milliard de voitures circulent sur Terre depuis 2010, plus de 4 milliards de personnes ont volé dans le ciel en 2017. « Cette agitation est-elle tenable ? Si non, comment nous déplacer demain ? ».
Entre autre, sont fait les constats suivants : Les plus grands adeptes des 4 roues restent les Européens avec 253millions de voitures pour 512,6millions d’habitants. / 80 % des déplacements des Français se font en voiture, occupée la plupart du temps par une seule personne. / La voiture reste le transport favori des Français : 67 % pour aller au travail ; 86 % pour faire des courses ; 69 % pour accompagner ses enfants à des activités quotidiennes. 50 % des trajets effectués font ainsi moins de 5 kilomètres. / Et pour clôturer le tout : Une voiture coûte plus de 5 000€ par an en moyenne tout en restant plus de 95 % de son temps en stationnement.</p>
            <p class="card-text text-justify">Pourtant… 65 % des Français se disent près à utiliser davantage les transports en commun, 40 % à faire du covoiturage et de l’autopartage.</p>
            <p class="card-text text-justify">Les alternatives possibles pour impulser ses changements : la pratique du vélo sur des pistes cyclables sécurisées et continues, les transports en commun en ville, le train pour les trajets plus longs. Idéalement, la multimodalité qui consiste à mixer les modes de transports.</p>
            <p class="card-text text-justify">Et finalement… Si l’on est obligé d’utiliser la voiture : pensons à privilégier le covoiturage pour que les véhicules soient occupées au maximum et à partager sa voiture en la louant à des particuliers afin d’éviter de la laisser stationner à longueur de journée (des sites comme Ouicar ou Drivy propose ce service).</p>
            <p class="card-text text-justify">Impulser le changement de nos modes de déplacement auraient alors des retombées directes sur notre santé, notre environnement, notre budget et notre économie… Ne serait-ce pas aussi l’occasion d’économiser 10 millions d’euros (soit le prix du parking souterrain prévu en plein Cœur de Ville) pour investir dans des mobilités vraiment durables ?</p>
          </div>
          <div class="card-footer text-center">
            <a href="http://www.qqf.fr/infographie/60/la-mobilite-de-demain?fbclid=IwAR2RWnyg-pYgRbUkvC3FWUeUpuPmCIMfluNgXSFEjrre8UHvDusDLM6nEc8" target="_blank" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>
      </div>
    </div>
    <!-- /.l'autre mobilité -->


  </div>
  <!-- /.container -->
