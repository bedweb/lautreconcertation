  <?php
  include('call_to_action.php');
  ?>
  <!-- Page Content -->
  <div class="container">
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="display-4">Que va devenir la place du 11 novembre ?</h1>
        <p class="lead">Savez-vous où en est le projet ? Voici un extrait du projet de réaménagement de la Place du 11 Novembre envisagé par la municipalité. On y voit l'emprise au sol du bâtiment, le parking souterrain et on devine la disparition des arbres. Cette page vous informera sur la définition du projet et de son avancement.</p>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6 mb-6">
          <div class="card h-100">
            <h3 class="card-header">Un bâtiment commercial de 6000 m²</h3>
            <div class="card-body">
              <img class="img-fluid rounded" src="img/leur-projet-1.jpg" alt=""/>
            </div>
            <ul class="list-group list-group-flush text-justify">
              <li class="list-group-item">La municipalité envisage la construction d'un îlot commercial d'une emprise au sol de 3600 m² sur les 9000 m² qu'offre la place du 11 novembre.</li>
              <li class="list-group-item">Pour comparaison, la salle polyvalente fait 3700 m².</li>
              <li class="list-group-item">Le complexe sur 2 étage fera 13 mètres de haut et offrira 6000 m² de nouvelle surface commerciale.</li>
              <li class="list-group-item">L'objectif annoncé de ce bâtiment est de redynamiser les commerces du centre-ville.</li>
            </ul>
          </div>
        </div>
        <div class="col-lg-6 mb-6">
          <div class="card card-outline-primary h-100">
            <h3 class="card-header">Un parking souterrain en plein coeur de ville</h3>
            <div class="card-body">
              <img class="img-fluid rounded" src="img/leur-projet-2.jpg" alt=""/>
            </div>
            <ul class="list-group list-group-flush text-justify">
              <li class="list-group-item">Il est également envisagé un parking souterrain en plein coeur de ville. Pour y accéder, les véhicules continueront de graviter autour de la place, contraignant la circulation des piétons, cyclistes et autres mobilités douces.</p></li>
              <li class="list-group-item">Ce parking pourra accueillir environ 370 véhicules. Le montant annoncé est de 28.000€ par place. Nous vous laissons faire le calcul...</li>
              <li class="list-group-item">Le choix d'un tel investissement ne semble pas en concordance avec l'urgence de la transition écologique.</li>
             </ul>
          </div>
        </div>
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->
